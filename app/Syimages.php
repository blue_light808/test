<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Syimages extends Model
{
    protected $table = 'syimages';
    protected $columns = ['img_id','img_name'];
    protected $primaryKey = 'img_id';
    public $timestamps = false;
    public $incrementing = false;
}

?>
