<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable // Model name used for eloqeunt and auth not table name.
{
    use Notifiable;
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'uid','uname','password','email','ulevel','created_date',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];
    protected $primaryKey = 'uid';
    // public incrementing = false; // disable if not using id->increment.
    // protected $table = 'users';
    // protected $guard = '';
    // public timestamps = false; // removed timestamps from migration.
}
