<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Emp_master extends Model
{
    protected $table = 'emp_master';
    protected $columns = ['eid','name','birth_date','birth_place','address','city','religion','nationality','job_desc','daily_salary','photo_id','login_id'];
    protected $primaryKey = 'eid';
    protected $timestamps = false;
    protected $incrementing = false;
}
