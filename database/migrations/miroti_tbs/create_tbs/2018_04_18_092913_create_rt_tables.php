<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRtTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('syimages', function(Blueprint $tbc) {
            $tbc->string('img_id', 8)->primary();
            $tbc->string('img_name', 50);
        });

        Schema::create('users', function(Blueprint $tbc) {
            $tbc->string('uid', 10)->primary();
            $tbc->string('uname', 30);
            $tbc->string('password', 80); // auth default harcoded field name, needs 60 bytes long
            $tbc->string('email', 30);
            $tbc->string('ulevel', 30);
            $tbc->dateTime('created_date');
            $tbc->rememberToken();
        });

        Schema::create('emp_master', function(Blueprint $tbc) {
            $tbc->string('eid', 6)->primary();
            $tbc->string('name', 35);
            $tbc->date('birth_date');
            $tbc->string('birth_place', 60);
            $tbc->string('address', 125);
            $tbc->string('city', 35);
            $tbc->string('religion', 40);
            $tbc->string('nationality', 65);
            $tbc->string('job_desc', 35);
            $tbc->decimal('daily_salary', 6, 2);
            $tbc->string('photo_id', 8);
            $tbc->string('login_id', 10);
            $tbc->index('photo_id');
            $tbc->index('login_id');
            $tbc->foreign('photo_id')->references('img_id')->on('syimages')->onDelete('cascade');
            $tbc->foreign('login_id')->references('uid')->on('users')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('emp_master', function(Blueprint $tbd) {
            $tbd->dropIndex('lists_user_id_index');
            $tbd->dropForeign('lists_user_id_foreign');
            $tbd->dropColumn('photo_id');
            $tbd->dropColumn('login_id');
        });
        
        Schema::dropIfExists('syimages');
        Schema::dropIfExists('users');
        Schema::dropIfexists('emp_master');
    }
}
