<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//Routes for main pages.
Route::get('/', 'linksas@index');
Route::get('welcome', 'linksas@welcome');
Route::get('hello_world', 'linksas@hello_world');
Route::get('dawnskies', 'linksas@dawnskies');
Route::get('cloudyair', 'linksas@cloudyair');

//Routes for eloquent queries (CURD) for sysusers.
Route::get('ryusers_insertion', 'sqlenginex@sysusersadd');
Route::get('rysers_modification', 'sqlenginex@sysusersmod');
Route::get('ryusers_deletion', 'sqlenginex@sysusersdel');
Route::get('ryusers_selection', 'sqlenginex@sysuserssel');

//Routes for adding & showing images.
Route::post('fileUpload', 'sqlenginex@photoUpload');
Route::get('fileStorage', 'sqlenginex@photoShow');
Route::get('fileUpdate', 'sqlenginex@photoUpdate');

//Routes for Login
Route::prefix('rylogin')->group(function()
{
Route::get('/', 'yuna@index')->name('login');
Route::post('/verifylogin', 'yuna@verflog')->name('login.verification');
Route::get('/logout', 'yuna@logout')->name('logout');
});

?>